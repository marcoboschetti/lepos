GLOBAL int_08_hand
GLOBAL _lidt
GLOBAL haltcpu

GLOBAL _irq00Handler
GLOBAL _irq01Handler
GLOBAL picMasterMask
GLOBAL picSlaveMask
GLOBAL _sti
GLOBAL _cli
GLOBAL _sysCallHandler

EXTERN int_t_tick
EXTERN int_t_keyboard
EXTERN sys_call_selector

%macro pushaq 0
    push rax      ;save current rax
    push rbx      ;save current rbx
    push rcx      ;save current rcx
    push rdx      ;save current rdx
    push rbp      ;save current rbp
    push rdi       ;save current rdi
    push rsi       ;save current rsi
    push r8        ;save current r8
    push r9        ;save current r9
    push r10      ;save current r10
    push r11      ;save current r11
    push r12      ;save current r12
    push r13      ;save current r13
    push r14      ;save current r14
    push r15      ;save current r15
%endmacro

%macro popaq 0
        pop r15
        pop r14
        pop r13
        pop r12
        pop r11
        pop r10
        pop r9
        pop r8
        pop rsi
        pop rdi
        pop rbp
        pop rdx
        pop rcx
        pop rbx
        pop rax
%endmacro

;8254 Timer (Timer Tick)
_irq00Handler:
	push rbp	
	mov rbp,rsp

	pushaq
	call int_t_tick
	mov al,20h
	out 20h, al
	popaq

	mov rsp,rbp
	pop rbp
	iretq

;Keyboard
_irq01Handler:
	push rbp	
	mov rbp,rsp

	pushaq

	call int_t_keyboard

	mov al,20h
	out 20h, al

	popaq

	mov rsp,rbp
	pop rbp
	iretq

;SYS_CALLS
_sysCallHandler:
    push rbp    
    mov rbp,rsp

    call sys_call_selector

    mov rsp,rbp
    pop rbp
    iretq



picMasterMask:
	push 	rbp
    mov 	rbp, rsp
  	xor		rax,rax
    out 	21h, al
    mov		rsp,rbp
    pop 	rbp
    retn

picSlaveMask:
	push    rbp
    mov     rbp, rsp
   	xor		rax, rax
    out		0A1h,al
    mov		rsp,rbp
    pop     rbp
    retn

 _cli:
	cli
	ret


_sti:
	sti
	ret