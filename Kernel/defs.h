
#ifndef _defs_
#define _defs_


/* Flags para derechos de acceso de los segmentos */
#define ACS_PRESENT     0x80            /* segmento presente en memoria */
#define ACS_CSEG        0x18            /* segmento de codigo */
#define ACS_DSEG        0x10            /* segmento de datos */
#define ACS_READ        0x02            /* segmento de lectura */
#define ACS_WRITE       0x02            /* segmento de escritura */
#define ACS_IDT         ACS_DSEG
#define ACS_INT_386 	0x0E		/* Interrupt GATE 32 bits */
#define ACS_INT         ( ACS_PRESENT | ACS_INT_386 )


#define ACS_CODE        (ACS_PRESENT | ACS_CSEG | ACS_READ)
#define ACS_DATA        (ACS_PRESENT | ACS_DSEG | ACS_WRITE)
#define ACS_STACK       (ACS_PRESENT | ACS_DSEG | ACS_WRITE)


void loadIDT();
void int_08();
void setupIDTEntry (int , uint16_t, uint64_t , uint8_t);

void int_t_tick();
void int_t_keyboard();

void get_tic();
void get_input();
void restoreScreen();
void setScreenSaver();
void saveScreen();
void printScreenSaver();
void sys_set_screen_saver(int seconds);

char codeToAscii(char code);
void initialize_keyboard();
void get_char();
char give_char();

void sys_write_char(char character,char form);
void sys_write_string(char* sebaString,char form);
void sys_write_scroll();
void clearScreen();
int getX();
void setX(int i);
int getY();
void setY(int j);

void sys_call_selector(int rdi, int rsi, int rdx);
void _sysCallHandler();
#endif