#include <stdint.h>
#include <string.h>
#include <lib.h>
#include <moduleLoader.h>
#include <naiveConsole.h>
#include "./asm/interrupts.h"
#include "types.h"
#include "defs.h"

void int_t_tick(){
	get_tic();
}

void int_t_keyboard(){
	get_input();
	get_char();
}