#include <stdint.h>
#include <string.h>
#include <lib.h>
#include <moduleLoader.h>
#include <naiveConsole.h>
#include "./asm/interrupts.h"
#include "types.h"
#include "defs.h"

#define SCREEN_SIZE 80*25*2
#define VIDEO_ADDR (char *) 0xB8000
static char *screen = VIDEO_ADDR;

/*
	total_time is in timer_tic units
*/

static long cont=0;
static long total_time=10000;
static char savedScreen[SCREEN_SIZE];
static int x,y;


void get_tic(){
	if(cont<total_time){
	cont+=1;
	}
	if(cont==total_time){
		cont++;
		setScreenSaver();
	}
	
}

void get_input(){
	if(cont>total_time){
		restoreScreen();
	}
	cont=0;
}

void setScreenSaver(){
	saveScreen();
	printScreenSaver();
	sys_write_char('\n', 0);
}

void saveScreen(){
	x=getX();
	y=getY();
	int i;
	for(i=0;i<SCREEN_SIZE;i++){
		savedScreen[i]=screen[i];
		screen[i]=0;
	}
}

void restoreScreen(){
	setX(x);
	setY(y);
	int i;
	for(i=0;i<SCREEN_SIZE;i++){
		screen[i]=savedScreen[i];
	}
}

void sys_set_screen_saver(int seconds){
	total_time=	seconds*1000/55;
	sys_write_char('\n', 0); 
	return;
}


void printScreenSaver(){
setX(0);
setY(0);
int fmt=12;
int fmt2=15;
sys_write_string("              .--.  .-\"     \"-.  .--.",fmt);
setX(0);
setY(getY()+1);
sys_write_string("             / .. \\/  .-. .-.  \\/ .. \\",fmt);
setX(0);
setY(getY()+1);
sys_write_string("            | |  '|  /   Y   \\  |'  | |",fmt);
setX(0);
setY(getY()+1);
sys_write_string("            | \\   \\  \\ 0 | 0 /  /   / |",fmt);
setX(0);
setY(getY()+1);
sys_write_string("             \\ '- ,\\.-\"`` ``\"-./, -' /",fmt);
setX(0);
setY(getY()+1);
sys_write_string("              `'-' /_   ^ ^   _\\ '-'`",fmt);
setX(0);
setY(getY()+1);
sys_write_string("              .--'|  \\._ _ _./  |'--.",fmt);
sys_write_string("         _              ___  __   ",fmt2);
setX(0);
setY(getY()+1);
sys_write_string("            /`    \\   \\.-.  /   /    `\\",fmt);
sys_write_string("      | | ___ _ __   /___\\/ _\\   ",fmt2);
setX(0);
setY(getY()+1);
sys_write_string("           /       '._/  |-' _.'       \\",fmt);
sys_write_string("     | |/ _ \\ '_ \\ //  //\\ \\    ",fmt2);
setX(0);
setY(getY()+1);
sys_write_string("          /          ;  /--~'   |       \\",fmt);
sys_write_string("    | |  __/ |_) / \\_// _\\ \\   ",fmt2);
setX(0);
setY(getY()+1);
sys_write_string("         /        .'\\|.-\\--.     \\       \\",fmt);
sys_write_string("   |_|\\___| .__/\\___/  \\__/   ",fmt2);
setX(0);
setY(getY()+1);
sys_write_string("        /   .'-. /.-.;\\  |\\|'~'-.|\\       \\",fmt);
sys_write_string("         |_|      ",fmt2);
setX(0);
setY(getY()+1);
sys_write_string("        \\       `-./`|_\\_/ `     `\'.      \\",fmt);
setX(0);
setY(getY()+1);
sys_write_string("         '.      ;     ___)        '.`;    /",fmt);
setX(0);
setY(getY()+1);
sys_write_string("           '-.,_ ;     ___)          \\/   /",fmt);
setX(0);
setY(getY()+1);
sys_write_string("            \\   ``'------'\\       \\   `  /",fmt);
setX(0);
setY(getY()+1);
sys_write_string("             '.    \\       '.      |   ;/_",fmt);
setX(0);
setY(getY()+1);
sys_write_string("           ___>     '.       \\_ _ _/   ,  '--.",fmt);
setX(0);
setY(getY()+1);
sys_write_string("         .'   '.   .-~~~~~-. /     |--'`~~-.  \\",fmt);
setX(0);
setY(getY()+1);
sys_write_string("        // / .---'/  .-~~-._/ / / /---..__.'  /",fmt);
setX(0);
setY(getY()+1);
sys_write_string("       ((_(_/    /  /      (_(_(_(---.__    .'",fmt);
	setX(0);
setY(getY()+1);
sys_write_string("                 | |     _              `~~`",fmt);
setX(0);
setY(getY()+1);
sys_write_string("                 | |     \'.",fmt);
setX(0);
setY(getY()+1);
sys_write_string("                  \\ '....' |",fmt);
setX(0);
setY(getY()+1);
sys_write_string("                   '.,___.'",fmt);

}
