#include <stdint.h>
#include <string.h>
#include <lib.h>
#include <moduleLoader.h>
#include <naiveConsole.h>
#include "./asm/interrupts.h"
#include "types.h"
#include "defs.h"
#include "./asm/Time.h"

/*
char sys_read_char(); <-1
void sys_write_char(char c); <-2
void sys_write_string(char * string, char form);<-3
void clear(); <-4
void set_ssaver_time(long seconds); <-5
char get_time(char selector);<-6
void set_time(char selector,char value);<-7
int sys_get_video_x(); <-8
int sys_get_video_y(); <-9
void sys_set_video_x(); <-10
void sys_set_video_y(); <-11
void sys_video_limit_scroll(int x, int y); <-12
void cli();<-13
void sti();<-14
*/
void sys_call_selector(int rdi, int rsi, int rdx){
	switch(rdi){
		case 1:
			give_char();
			return;
		case 2:
			sys_write_char(rsi,rdx);
			return;
		case 3:
			sys_write_string(rsi,rdx);
			return;
		case 4:
			clearScreen();
			return;
		case 5:
			sys_set_screen_saver(rsi);
			return;
		case 6:
			get_time(rsi);
			return;
		case 7:
			set_time(rsi,rdx);
			return;
		case 8:
			getX();
			return;
		case 9:
			getY();
			return;
		case 10:
			setX(rsi);
			return;
		case 11:
			setY(rsi);
			return;
		case 12:
			set_video_scrollLimit(rsi,rdx);
			return;
		case 13:
			_cli();
			return;
		case 14:
			_sti();
			return;
		default: sys_write_string("Error 1002: INTERRUPCION INVALIDA",12);
	}
}
