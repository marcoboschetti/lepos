#define ROWS 25
#define COLUMNS 80
#define VIDEO_ADDR (char *) 0xB8000;
#define BACKSPACE 0x0E
#define ENTER 10


static int x=0;
static int y=0;

static int minScroll=0;
static int maxScroll=25;

void sys_write_char(char character,char form);
void sys_write_string(char* sebaString,char form);
void sys_write_scroll();

char *screen = VIDEO_ADDR;


void sys_write_char(char character,char form)
{

	if(character==BACKSPACE){
		if(x==0){
			if(y!=minScroll){
				y--;
				x=COLUMNS-1;
			}
		}
		else{
			x--;
		}	
		screen[(y*COLUMNS + x)*2] = ' ';
	}
	else if(character==ENTER){
		if(y==maxScroll - 1){
			sys_write_scroll();
		}
		y++;
		x=0;
	}


	else{
		screen[(y*COLUMNS + x)*2] = character;
		screen[(y*COLUMNS + x)*2+1] = form;
		x++;
		if(x==COLUMNS){
			y++;
			x=0;
			if(y==maxScroll){
				sys_write_scroll();
			}
		}
	}
	return;
}

void sys_write_string(char * string, char form){
	int i=0;
	for (i = 0; string[i] != 0; i++)
		sys_write_char(string[i],form);
}

void sys_write_scroll(){
	int i,j;
	for(i=0;i<COLUMNS;i++){
		for(j=minScroll + 1;j<maxScroll;j++){
			screen[((j-1)*COLUMNS + i)*2] = screen[(j*COLUMNS + i)*2] ;
			screen[((j-1)*COLUMNS + i)*2+1] = screen[(j*COLUMNS + i)*2+1];
		}
	}

	for(i=0;i<COLUMNS;i++){
		screen[((j-1)*COLUMNS + i)*2] = 0 ;
		screen[((j-1)*COLUMNS + i)*2+1] = 0;	
	}

	if(y!=0){
		y--;
	}
}


void clearScreen(){
	int i;
	for(i=(minScroll+1)*COLUMNS;i<(maxScroll)*COLUMNS*2;i++){
		screen[i]=12;
	}
	x=0;
	y=minScroll;
}

int getX(){
	return x;
}

void setX(int i){
	x=i;
}

int getY(){
	return y;
}

void setY(int j){
	y=j;
}

void set_video_scrollLimit(int min, int max){
	minScroll=min;
	maxScroll=max;
	return;
}