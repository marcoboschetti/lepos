#include <stdint.h>
#include "sys_calls.h"
#include "defs.h"

extern char bss;
extern char endOfBinary;

#define STRING 		0
#define INT 		1
#define CHAR 		2
#define FOOTER_SIZE	1
#define HEADER_SIZE	1
#define ROWS		25
#define COLUMNS		80
#define BLOCK		6

void * memset(void * destiny, int32_t c, uint64_t length);
int convertArg(char ** args, char * argTypes, int cant);
void intToArgType(int i, char * buffer);
void shell_set_hour(char* hour);
void shell_set_min(char* min);
void shell_set_sec(char* sec);

struct command
{
	char * name;
	void (* function) ();
	unsigned char * args;
	int argsCant;
	char* desc;
};


static struct command commands[COM] = {};

int main() {
sys_write_char('x',12);
sys_write_char('x',12);
sys_write_char('x',12);
sys_write_char('x',12);
sys_write_char('x',12);
sys_write_char('x',12);
sys_write_char('x',12);
sys_write_char('x',12);
	//Clean BSS
//	memset(&bss, 0, &endOfBinary - &bss);

	updateGMT();

	char buffer[300];
	char lastTime[10];
	char currentTime[10];
	unsigned char c;
	int 
	index, 
	flag, 
	i, 
	inputArgs, 
	cont, 
	cursorCont,
	cursorFlag,
	fmt;

	sys_video_limit_scroll(FOOTER_SIZE, ROWS - HEADER_SIZE);

	for(i=0; i< 80;i++)
		sys_write_char(' ', 112);

	sys_set_video_y(0);	
	sys_set_video_x(35);
	sys_write_string("lep", 127);
	sys_write_string("OS", 114);

	sys_set_video_x(0);
	sys_set_video_y(24);

	for(i=0; i< 80;i++){
		if(i<70)
			sys_write_char(' ', 112);
		else
			sys_write_char(' ', 32);	
	}
	get_time(lastTime);
	updateShellTime();
	sys_set_video_y(1);
	sys_set_video_x(0);
	sys_write_string("XXDEBUG",15);
	unsigned char clearArgs[] = {};
	commands[0].name = "clear";
	commands[0].function = &clear;
	commands[0].args = clearArgs;
	commands[0].argsCant = 0;
	commands[0].desc = "Clears the screen, restoring it to its purest state";

	unsigned char echoArgs[] = {STRING, CHAR};
	commands[1].name = "echo";
	commands[1].function = &echo;
	commands[1].args = echoArgs;
	commands[1].argsCant = 2;
	commands[1].desc = "Prints a given string with the provided format.";

	unsigned char getTimeArgs[] = {};
	commands[2].name = "getTime";
	commands[2].function = &print_time;
	commands[2].args = getTimeArgs;
	commands[2].argsCant = 0;
	commands[2].desc = "Gives the current Time in this format:  HH:MM:SS";

	unsigned char setTimeArgs[] = {STRING,STRING,STRING};
	commands[3].name = "setTime";
	commands[3].function = &set_time;
	commands[3].args = setTimeArgs;
	commands[3].argsCant = 3;
	commands[3].desc = "Sets the system's time to the hours, time and seconds given.";

	unsigned char setSecArgs[] = {STRING};
	commands[4].name = "setSec";
	commands[4].function = &shell_set_sec;
	commands[4].args = setSecArgs;
	commands[4].argsCant = 1;
	commands[4].desc = "Sets de system seconds to the ones provided.";

	unsigned char setMinArgs[] = {STRING};
	commands[5].name = "setMin";
	commands[5].function = &shell_set_min;
	commands[5].args = setMinArgs;
	commands[5].argsCant = 1;
	commands[5].desc = "Sets de system minutes to the ones provided.";

	unsigned char setHourArgs[] = {STRING};
	commands[6].name = "setHour";
	commands[6].function = &shell_set_hour;
	commands[6].args = setHourArgs;
	commands[6].argsCant = 1;
	commands[6].desc = "Sets de system hours to the ones provided.";

	unsigned char setScreenSaverArgs[] = {INT};
	commands[7].name = "setScreenSaver";
	commands[7].function = &setScreenSaverTime;
	commands[7].args = setScreenSaverArgs;
	commands[7].argsCant = 1;
	commands[7].desc = "Sets the screen saver's delay to the amount of seconds provided";

	unsigned char getDateArgs[] = {};
	commands[8].name = "getDate";
	commands[8].function = &print_date;
	commands[8].args = getDateArgs;
	commands[8].argsCant = 0;
	commands[8].desc = "Provides the current date in -Day of the week DD/MM/YY- format.";

	unsigned char helpArgs[] = {};
	commands[9].name = "help";
	commands[9].function = &help;
	commands[9].args = helpArgs;
	commands[9].argsCant = 0;
	commands[9].desc = "Gives help on the general use of the system's functions.";

	unsigned char askArgs[] = {};
	commands[10].name = "ask";
	commands[10].function = &responde;
	commands[10].args = askArgs;
	commands[10].argsCant = 0;
	commands[10].desc = "Ask something.";

	unsigned char colorArgs[] = {};
	commands[11].name = "colors";
	commands[11].function = &colors;
	commands[11].args = colorArgs;
	commands[11].argsCant = 0;
	commands[11].desc = "Wanna see colors?";
	
	unsigned char hangArgs[] = {};
	commands[12].name = "hangman";
	commands[12].function = &hangman;
	commands[12].args = hangArgs;
	commands[12].argsCant = 0;
	commands[12].desc = "A classic hangman game";

	unsigned char dropsArgs[] = {};
	commands[13].name = "colorDrops";
	commands[13].function = &playColors;
	commands[13].args = dropsArgs;
	commands[13].argsCant = 0;
	commands[13].desc = "Try an aweasome game with colors!";

	while(1){
		i=0;
		flag = 1;
		cont = 200;
		cursorCont = 0;
		cursorFlag = 1;
		sys_write_string(":)",10);
		sys_write_string(" > ", 15);
		updateFooter();
		while((c=sys_read_char())!=10){

			get_time(currentTime);
			if(!strcmp(currentTime,lastTime)){
				get_time(lastTime);
				updateShellTime();
			}
			if(!cursorCont--){
				cursorCont = 50000;
				if(cursorFlag==1){
					fmt = 0;
					cursorFlag=0;
				}else{
					fmt = 32;
					cursorFlag=1;
				}
				sys_write_char(' ', fmt);
				sys_set_video_x(sys_get_video_x()-1);
			}
			if(c!=0){
				if(c==0x0E){
					if(i){
						i--;
						sys_set_video_x(sys_get_video_x()-1);
						sys_write_string("  ",0);
						sys_set_video_x(sys_get_video_x()-2);
					}
				}
				else{
					buffer[i++] = c;
					sys_write_char(c,10);
				}
				updateFooter();
			}

		}
		sys_write_char(' ',0);

		buffer[i]=0;
		char * args[10];
		inputArgs = scanf(buffer, args);
		strcmp(commands[0].name, args[0]);

		for(index = 0; flag && index<COM ; index++){
			if(strcmp(commands[index].name, args[0]) && commands[index].argsCant==inputArgs-1){
				flag=0;
				convertArg(args, commands[index].args, commands[index].argsCant);
				switch(commands[index].argsCant){
				case 0:
				 	commands[index].function();
					break;
				case 1:
					commands[index].function(args[1]);
					break;
				case 2: 
					commands[index].function(args[1], args[2]);
					break;
				case 3:
					commands[index].function(args[1], args[2], args[3]);
					break;
				}
			}
		}

		if(flag)
			sys_write_string("\n______________________________\n\nInvalid command \n______________________________\n", 15);
	}

	sys_write_string("XXDEBUG",15);
	return 0xDEADBEEF;
}

int convertArg(char ** args, char * argTypes, int cant){
	int  j, flag = 1;
	for(j=1; j<= cant && flag; j++){
		switch(argTypes[j-1]){
			case CHAR:
				args[j]=stringToInt(args[j]);
				break;
			case INT:
				args[j]=stringToInt(args[j]);
				break;
			default:
				break;
		}	
	}
	
}

void * memset(void * destiation, int32_t c, uint64_t length) {
	uint8_t chr = (uint8_t)c;
	char * dst = (char*)destiation;

	while(length--)
		dst[length] = chr;

	return destiation;
}

void help(){
	int i = 0;
	char * nm = "|->Command name: ";
	char * argC = "Arg count: ";
	char * argT = "  Arg types: ";
	char * spaces = "  ";
	char *dsc = "  |_Description:";
	char buffer[256];
	char c;
	char flag = 1;

	while (i < COM && flag){
		clear();
			new_line();
		do{
			new_line();
			sys_write_string(nm, 15);
			sys_write_string(commands[i].name, 11);
			sys_set_video_x(32);
			sys_write_string(argC,15);
			intToString(commands[i].argsCant, buffer);
			sys_write_string(buffer, 11);
			if (commands[i].argsCant)
				sys_write_string(argT,15);
			for(int j=0;j<commands[i].argsCant;j++){
				intToArgType(commands[i].args[j], buffer);
				sys_write_string(buffer, 11);
				sys_write_string(" ",15);
			}
			sys_write_string("  ",15);
			sys_write_char('\n',12);
			sys_write_string(dsc,15);
			sys_write_string(commands[i].desc,12);
			i++;
			new_line();
		}while((i%BLOCK)!=0 && i<COM);
		new_line();
		sys_set_video_y(23);
		printf(10, "For next page type \"N\", to exit type \"Q\".");
		while((c=toUpper(sys_read_char()))!='N' && c!='Q');
		if(c=='Q')
			flag = 0;
	}
	clear();
}

void intToArgType(int i , char * buffer){
	char * tocopy;
	switch(i){
		case 0:
			stringCopy(buffer, "STR");
			break;
		case 1:
			stringCopy(buffer, "INT");
			break;
		case 2:
			stringCopy(buffer, "CHAR");
			break;
	}
	
	return;
}

void colors(){
	new_line();
	char buffer[20];
	for(int i=0; i<16; i++){
		intToString(i, buffer);
		if(i<10 && i)
			sys_write_char(' ',10);
		printf(15, "   %s", buffer);
		//sys_write_string(buffer, 10);
	}
	sys_write_char('\n',12);

	for(int i=0; i<16; i++){
		sys_write_string("   X ",i);
	}

	new_line();
	for(int i=0; i<16*16; i+=16){
		intToString(i, buffer);
		if(i<100)
			sys_write_char(' ',10);
		printf(15,"  %s", buffer);
		//sys_write_string(buffer, 10);
	}
	sys_write_char('\n',12);

	for(int i=0; i<16*16; i+=16){
		sys_write_string("     ", i+1);
	}
	new_line();		
}

void updateShellTime(){
	cli();
	int x = sys_get_video_x();
	int y = sys_get_video_y();

	sys_set_video_y(24);	
	sys_set_video_x(71);
	print_simple_time(47);

	sys_set_video_x(x);
	sys_set_video_y(y);
	sti();
}

void updateFooter(){
	cli();
	int x = sys_get_video_x();
	int y = sys_get_video_y();

	sys_set_video_y(24);
	sys_set_video_x(10);
	printf(47, " X: %d Y: %d  ", x, y);

	sys_set_video_x(x);
	sys_set_video_y(y);
	sti();
}


void shell_set_hour(char* hour){
	new_line();
	set_hour(hour);
	return;
}
void shell_set_min(char* min){
	new_line();
	set_min(min);
	return;
}
void shell_set_sec(char* sec){
	new_line();
	set_sec(sec);
	return;
}