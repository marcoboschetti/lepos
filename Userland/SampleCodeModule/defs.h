/*
 Returns the string's length 
*/
#ifndef _defs_
#define _defs_

#define COM 	14

void updateFooter();
void updateShellTime();

int sys_read_line(char * buffer, int bufferSize);
void stringCopy(char * str1, char* str2);
void intToString(int number,char* string);

int stringToInt(char* str);

void concatString(char* str1, char* str2);

void appendChar(char*str1,char ch);

unsigned char reverse_translate_time(char * str);

void help();
void echo(char * str, char fmt);

void print_time();
void print_simple_time(char fmt);
void print_date();
void get_time(char* full);
void set_hour(char*hour);
void set_min(char*min);
void set_sec(char*sec);
void set_time(char * hour, char * min, char * sec);
void get_sec(char * buffer);
void get_min(char * buffer);
void get_hour(char * buffer);
void updateGMT(void);
void setScreenSaverTime(int i);
int getScreenSaverTime();


int scanf(char * string, char ** aux);
int stringCmp(char * s1, char * s2);
void new_line();
void dec_to_hex(int value, char * buffer);
int strlen(char* str);
int isalpha(char c);
char toUpper(char c);
char strcmp(char* s1, char*s2);

void get_day(char * buffer);
void get_month(char * buffer);
void get_year(char * buffer);
void get_date(char* buffer);
void get_weekday(char * buffer);

int rand();
void srand();
int randInt(int min, int max);

void hangman(void);
void responde();
void playColors();
void colors();

#endif