GLOBAL sys_read_char
GLOBAL sys_write_char
GLOBAL sys_write_string
GLOBAL clear
GLOBAL set_ssaver_time
GLOBAL sys_get_time
GLOBAL sys_set_time
GLOBAL get_argument
GLOBAL sys_get_video_x
GLOBAL sys_get_video_y
GLOBAL sys_set_video_x
GLOBAL sys_set_video_y
GLOBAL sys_video_limit_scroll
GLOBAL cli
GLOBAL sti

;pushaq and popaq ignores the rax register, used as return parameter

%macro pushaq 0
    push rbx      ;save current rbx
    push rcx      ;save current rcx
    push rdx      ;save current rdx
    push rbp      ;save current rbp
    push rdi       ;save current rdi
    push rsi       ;save current rsi
    push r8        ;save current r8
    push r9        ;save current r9
    push r10      ;save current r10
    push r11      ;save current r11
    push r12      ;save current r12
    push r13      ;save current r13
    push r14      ;save current r14
    push r15      ;save current r15
%endmacro

%macro popaq 0
        pop r15
        pop r14
        pop r13
        pop r12
        pop r11
        pop r10
        pop r9
        pop r8
        pop rsi
        pop rdi
        pop rbp
        pop rdx
        pop rcx
        pop rbx
%endmacro

%macro pushaqfull 0
    push rax
    push rbx      ;save current rbx
    push rcx      ;save current rcx
    push rdx      ;save current rdx
    push rbp      ;save current rbp
    push rdi       ;save current rdi
    push rsi       ;save current rsi
    push r8        ;save current r8
    push r9        ;save current r9
    push r10      ;save current r10
    push r11      ;save current r11
    push r12      ;save current r12
    push r13      ;save current r13
    push r14      ;save current r14
    push r15      ;save current r15
%endmacro

%macro popaqfull 0
        pop r15
        pop r14
        pop r13
        pop r12
        pop r11
        pop r10
        pop r9
        pop r8
        pop rsi
        pop rdi
        pop rbp
        pop rdx
        pop rcx
        pop rbx
        pop rax
%endmacro



section .text:

sys_read_char:

    push rbp    
    mov rbp,rsp

    pushaq

    mov rax, 1
    mov rdi,rax
    int 80h

    popaq

    mov rsp,rbp
    pop rbp

    ret

sys_write_char:
    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rdx,rsi
    mov rsi,rdi
    mov rax, 2
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret

sys_write_string:
    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rdx,rsi
    mov rsi,rdi
    mov rax, 3
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret

clear:

    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rax, 4
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret

set_ssaver_time:

    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rsi,rdi
    mov rax, 5
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret

sys_get_time:
    push rbp    
    mov rbp,rsp

    pushaq

    mov rsi,rdi
    mov rax, 6
    mov rdi,rax
    int 80h

    popaq

    mov rsp,rbp
    pop rbp

    ret

sys_set_time:
    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rdx,rsi
    mov rsi,rdi
    mov rax, 7
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret

sys_get_video_x:
    push rbp    
    mov rbp,rsp

    pushaq

    mov rax, 8
    mov rdi,rax
    int 80h

    popaq

    mov rsp,rbp
    pop rbp

    ret

sys_get_video_y:
    push rbp    
    mov rbp,rsp

    pushaq

    mov rax, 9
    mov rdi,rax
    int 80h

    popaq

    mov rsp,rbp
    pop rbp

    ret

sys_set_video_x:
    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rdx,rsi
    mov rsi,rdi
    mov rax, 10
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret

sys_set_video_y:
    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rdx,rsi
    mov rsi,rdi
    mov rax, 11
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret

sys_video_limit_scroll:
    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rdx,rsi
    mov rsi,rdi
    mov rax, 12
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret

cli:

    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rax, 13
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret

sti:

    push rbp    
    mov rbp,rsp

    pushaqfull

    mov rax, 14
    mov rdi,rax
    int 80h

    popaqfull

    mov rsp,rbp
    pop rbp

    ret