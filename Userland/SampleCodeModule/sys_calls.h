#ifndef _ints_
#define _ints_

/*
	Returns a char from the keyboard buffer, 
	or 0 if it's empty
*/
char sys_read_char();

void sys_write_char(char c, char form);

/*
	Requires a null terminated string (0)
*/
void sys_write_string(char * string, char form);

void clear();

/*
	Sets the screen saver time in the given seconds
*/
void set_ssaver_time(int seconds);

unsigned char sys_get_time(char selector);
void sys_set_time(char selector,char value);

//Video ints

int sys_get_video_x();
int sys_get_video_y();

void sys_set_video_x();
void sys_set_video_y();

void sys_video_limit_scroll(int min, int max);

void cli();
void sti();

#endif