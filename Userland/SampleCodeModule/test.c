#include "sys_calls.h"
#include "defs.h"
#define ENTER 10
int ejecutarSentencia();
void traducirAPantalla();
#define TRAP 'w'
#define NACCEPT "La pregunta no fue clara. Pregunta otra vez:"
#define FRASE_INICIAL "Estimado programa, me gustaria preguntarte algo,"

void responde(){
	int lastSSaverTime=getScreenSaverTime();
	set_ssaver_time(10000);
	new_line();
	sys_write_string("Bienvenido al preguntador automatico",11);
	sys_write_char(ENTER,15);
	sys_write_string("Para hacer una pregunta, simplemente tecleala",11);
	sys_write_char(ENTER,15);
	sys_write_string("Toda pregunta debe empezar con la siguiente frase,",11);
	sys_write_char(ENTER,15);
	sys_write_string("Estimado programa,",12);
	sys_write_char(ENTER,15);
	sys_write_string("O bien",15);
	sys_write_char(ENTER,15);	
	sys_write_string(FRASE_INICIAL,12);
	sys_write_char(ENTER,15);
	sys_write_string("seguida de tu pregunta.",11);
	sys_write_char(ENTER,15);
	sys_write_string("Ingresa cualquier numero en el primer caracter para salir.",12);
	sys_write_char(ENTER,15);
	sys_write_string("---------------------------------------",15);
	sys_write_char(ENTER,15);
	char resp=1;
	while(resp){
		resp=ejecutarSentencia();
	}
	set_ssaver_time(lastSSaverTime);
}



int ejecutarSentencia(){
	char buff[256];
	char flag=0;
	char key;

	while((key=sys_read_char())==0){}
	
	if(key==TRAP){
		flag=1;
		int i=0;
		sys_write_char(FRASE_INICIAL[i],15);
		while((key=sys_read_char())!=TRAP){
			if(key!=0){
				buff[i++]=key;
				sys_write_char(FRASE_INICIAL[i],15);
			}
		}
		sys_write_char(FRASE_INICIAL[i+1],15);
		buff[i]=0;
		traducirAPantalla();

	}else if(key<'0' || key>'9'){
		sys_write_char(key,15);
		traducirAPantalla();
	}else{
		return 0;
	}

	if(!flag){
		sys_write_char(ENTER,15);
		sys_write_string(NACCEPT,14);
		sys_write_char(ENTER,15);
	}else{
		sys_write_char(ENTER,15);
		sys_write_string(buff,14);
		sys_write_char(ENTER,15);		
	}
	return 1;
}

	void traducirAPantalla(){
		char key;
		while((key=sys_read_char())!=ENTER){
			updateShellTime();
			if(key!=0)
			sys_write_char(key,15);		
		}
	}
