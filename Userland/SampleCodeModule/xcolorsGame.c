#include "defs.h"
#include "sys_calls.h"

#define MIN_TIME 3

struct block
{
	char color, x, y;
};

static int highScore=0;

void playColors(){
	clear();
	int lastSSaverTime=getScreenSaverTime();
	set_ssaver_time(10000);

	struct block blocks[20];
	char c, initX = 2, initY=18, sourceY=5;
	int i, score = 0, inf, sup, flag = 1, k, l, acu, time = 3;
	char buffer[20];
	char aux[20];
	srand();
	for(int j = 0; j<20; j++){
		blocks[j].color = 0;
	}

	sys_set_video_x(1);
	sys_set_video_y(20);
	printf(12, "Score: %d\n", score);
	sys_set_video_x(1);
	printf(12, "Global Highscore: %d", highScore);
	
	for(int t = 1; t<=sourceY;t++){
		sys_set_video_y(t);
		sys_set_video_x(0);
		for(int m = 0; m<80; m++)
			sys_write_char(' ', 15*16);
	}

	while(flag){
		if((c=sys_read_char())=='a'){
			initX--;
			if(initX==1){
				initX=77;
			}
		}else if(c=='d'){
			initX++;
			if(initX==78)
				initX=2;
		}
		get_time(buffer);
		if(!strcmp(aux, buffer)){
			stringCopy(aux, buffer);

			if(!time--){
				time=rand()%3+5-(score/5);
				time=((time<MIN_TIME)?MIN_TIME:time);
				k = 0;
				while(k++<20 && (blocks[k].color)!=0);
				sys_set_video_y(21);
				if(k!=20){
					blocks[k].color=rand()%15+1;
					blocks[k].y=sourceY;
					blocks[k].x=rand()%75+2;
				}
				sys_set_video_x(1);
				sys_set_video_y(22);
					
			}

			for(int j = 0; j<20; j++){
				if(blocks[j].color!=0){
					blocks[j].y++;
					if(blocks[j].y==initY){
						if(((blocks[j].x < (5*blocks[j].color+initX)) &&
						 (blocks[j].x >= (5*(blocks[j].color-1)+initX)))||
							((blocks[j].x < (5*blocks[j].color+initX-75)) &&
						 (blocks[j].x >= (5*(blocks[j].color-1)+initX-75)))){
							score++;
							if(score>highScore){
								highScore=score;
							}
							sys_set_video_x(1);
							sys_set_video_y(20);
							printf(12, "Score: %d\n", score);
							sys_set_video_x(1);
							printf(12, "Global Highscore: %d", highScore);
						}else{
							flag = 0;							
						}
						blocks[j].color = 0;
					}
					sys_set_video_y(blocks[j].y);
					sys_set_video_x(blocks[j].x);
					sys_write_char(' ', blocks[j].color * 16);
					if((blocks[j].y-1)!=sourceY){
						sys_set_video_y(blocks[j].y-1);		
						sys_set_video_x(blocks[j].x);
						sys_write_char(' ',0);
					}
				
				}
				
			}	
		}

		sys_set_video_y(initY);
		sys_set_video_x(initX);
		for(i=5; i<82-initX; i++){
			sys_write_char(' ', (i/5)*16);
		}
		sys_set_video_x(2);
		for(int j=2; j<initX; j++){
			sys_write_char(' ', (i++/5)*16);
		}
	}
	clear();
	set_ssaver_time(lastSSaverTime);
	clear();
}